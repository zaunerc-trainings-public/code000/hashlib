package hash

import (
	"fmt"
	"math/rand"
)

func Generate(seed int64) string {
	rand.Seed(seed)
	return fmt.Sprintf("%d", rand.Int())
}
